﻿using Mono.Nat;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace VPNDataProxy
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GetAddresses();
            NatUtility.DeviceFound += NatUtility_DeviceFound;
            NatUtility.Search(IPAddress.Parse(gatewayAddress), NatProtocol.Pmp);
            Thread.Sleep(1000);

            //  StartTcpListener("10.96.0.16", 15655);

            Stopwatch sw = Stopwatch.StartNew();
            var first = true;
            while (true)
            {
                if (first || sw.Elapsed > TimeSpan.FromSeconds(30))
                {
                    first = false;
                    foreach (var dev in devices)
                    {
                        foreach (var desiredMapping in desiredMappings)
                        {
                            var mapping = dev.CreatePortMap(desiredMapping);
                            Console.WriteLine(mapping.ToString());

                            if (mapping.PublicPort != desiredMapping.PublicPort)
                            {
                                RelayConfig relayConfig = new RelayConfig(mapping.Protocol, mapping.PrivatePort, interfaceAddress, mapping.PublicPort, interfaceAddress);
                                if (!relays.ContainsKey(relayConfig.GetHash()))
                                {
                                    Thread t = new Thread((_) =>
                                    {
                                        switch (mapping.Protocol)
                                        {
                                            case Protocol.Tcp:
                                                var rt = new RelayTCP(relayConfig);
                                                rt.Run();
                                                break;
                                            case Protocol.Udp:
                                                var ru = new RelayUDP(relayConfig);
                                                ru.Run();
                                                break;
                                        }
                                    });
                                    t.Start();
                                    relays.Add(relayConfig.GetHash(), t);
                                }
                            }
                        }
                    }
                    sw.Restart();
                }
                Thread.Sleep(100);
            }
        }

        private static void NatUtility_DeviceFound(object sender, DeviceEventArgs e)
        {
            Console.WriteLine(e.Device.ToString());
            devices.Add(e.Device);
        }

        private static void GetAddresses()
        {
            var k = NetworkInterface.GetAllNetworkInterfaces();

            var y = NetworkInterface
               .GetAllNetworkInterfaces()
               .Where(n => n.OperationalStatus == OperationalStatus.Up)
               .Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
               .Where(a => ((int)a.NetworkInterfaceType == 53) || a.Name.ToUpper().Contains("OPENVPN") || a.Name.ToUpper().Contains("PROTON"));

            if (y.Count() != 1)
            {
                throw new Exception($"Found either zero or more than one potential network interfaces when searching for ProtonVPN/OpenVPN interface.  Aborting.");
            }

            var _interface = y.First();
            var ipProps = _interface.GetIPProperties();
            interfaceAddress = ipProps.UnicastAddresses.First(b => b.Address.AddressFamily == AddressFamily.InterNetwork).Address.ToString();
            gatewayAddress = ipProps.DnsAddresses.First(b => b.AddressFamily == AddressFamily.InterNetwork).ToString();
        }
        private static string interfaceAddress = "";
        private static string gatewayAddress = "";

        private static List<INatDevice> devices = new List<INatDevice>();
        private static List<Mapping> desiredMappings = new List<Mapping>() { new Mapping(Protocol.Tcp, 15653, 15653), new Mapping(Protocol.Udp, 15653, 15653) };

        private static Dictionary<string, Thread> relays = new Dictionary<string, Thread>();
    }
}
