﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace VPNDataProxy
{
    class RelayTCP
    {
        public int listenPort;
        public string listenHost;
        public int targetPort;
        public string targetHost;

        public RelayTCP(RelayConfig cfg)
        {
            this.listenHost = cfg.listenHost;
            this.listenPort = cfg.listenPort;
            this.targetHost = cfg.targetHost;
            this.targetPort = cfg.targetPort;
        }
        class SocketPair
        {
            ConcurrentDictionary<int, SocketPair> pool { get; set; } = null;

            TcpClient remoteClient { get; set; }
            TcpClient appClient { get; set; }

            NetworkStream remoteStream { get; set; }
            NetworkStream appStream { get; set; }

            Queue<IEnumerable<byte>> ForRemote { get; set; } = new Queue<IEnumerable<byte>>();
            Queue<IEnumerable<byte>> ForApp { get; set; } = new Queue<IEnumerable<byte>>();

            public SocketPair(TcpClient remote, TcpClient app, ConcurrentDictionary<int, SocketPair> pool)
            {
                remoteClient = remote;
                appClient = app;
                this.pool = pool;
            }

            public void Run()
            {
                pool[Thread.CurrentThread.ManagedThreadId] = this;

                var connectionCount = pool.Where(k => k.Value != null).Count();
                Console.WriteLine($"TCP connection count is {connectionCount}");
                remoteStream = remoteClient.GetStream();
                appStream = appClient.GetStream();
                var waitRamp = TimeSpan.FromMilliseconds(50);
                try
                {
                    int wait = 0;
                    Stopwatch sw = Stopwatch.StartNew();
                    Stopwatch sw2 = Stopwatch.StartNew();
                    while (true)
                    {

                        var a = Read(remoteStream, ForApp, remoteClient.Client.RemoteEndPoint);
                        var b = Write(appStream, ForApp);
                        var c = Read(appStream, ForRemote, appClient.Client.RemoteEndPoint);
                        var d = Write(remoteStream, ForRemote);

                        if (a || b || c || d)
                        {
                            wait = 0;
                            sw.Restart();
                            sw2.Restart();
                        }
                        else
                        {
                            if (sw2.Elapsed > waitRamp)
                            {
                                wait = Math.Min(1 + wait * 2, 200);
                                sw2.Restart();
                            }
                            if (sw.Elapsed > TimeSpan.FromSeconds(900))
                            {
                                Console.WriteLine($"closing idle TCP connection");
                                break;
                            }
                        }
                        if (wait > 0)
                        {
                            Thread.Sleep(wait);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    try { remoteStream.Close(); } catch (Exception ex) { }
                    try { remoteClient.Close(); } catch (Exception ex) { }
                    try { appStream.Close(); } catch (Exception ex) { }
                    try { appClient.Close(); } catch (Exception ex) { }
                }
                pool[Thread.CurrentThread.ManagedThreadId] = null;
            }
            private bool Read(NetworkStream readFrom, Queue<IEnumerable<byte>> writeTo, EndPoint from)
            {
                if (!readFrom.DataAvailable)
                    return false;
                var bytes = new byte[8192];
                var allBytes = new List<byte>();
                var i = readFrom.Read(bytes, 0, bytes.Length);
                while (i != 0)
                {
                    allBytes.AddRange(bytes.Take(i));
                    bytes = new byte[8192];
                    i = readFrom.DataAvailable ? readFrom.Read(bytes, 0, bytes.Length) : 0;
                }
                if (allBytes.Count > 0)
                {
                    Console.WriteLine($"got {allBytes.Count} TCP bytes from {from}");
                    writeTo.Enqueue(allBytes);
                    return true;
                }
                return false;
            }
            private bool Write(NetworkStream writeTo, Queue<IEnumerable<byte>> readFrom)
            {
                if (readFrom.Count < 1)
                    return false;
                while (readFrom.Count > 0)
                {
                    var bytes = readFrom.Dequeue().ToArray();
                    writeTo.Write(bytes, 0, bytes.Length);
                }
                return true;
            }
        }

        ConcurrentDictionary<int, SocketPair> pool = new ConcurrentDictionary<int, SocketPair>();

        public void Run()
        {
            TcpListener server = null;
            try
            {
                server = new TcpListener(IPAddress.Any, this.listenPort);
                Console.WriteLine($"TCP listening on {server.LocalEndpoint}");
                server.Start();
                while (true)
                {
                    var client = server.AcceptTcpClient();
                    try
                    {
                        SocketPair pair = new SocketPair(client, new TcpClient(this.targetHost, this.targetPort), pool);
                        Thread t = new Thread(() => { pair.Run(); });
                        t.Start();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                server.Stop();
            }
        }
    }
}
